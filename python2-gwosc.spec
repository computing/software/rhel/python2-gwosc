%define name    python2-gwosc
%define sname   gwosc
%define version 0.4.3
%define release 3

Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Summary:   A python interface to the Gravitational-Wave Open Science Center data archive

License:   GPLv3
Url:       https://pypi.org/project/%{sname}/
Source0:   https://pypi.io/packages/source/g/%{sname}/%{sname}-%{version}.tar.gz

Vendor:    Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch

# rpmbuild dependencies
BuildRequires: rpm-build
BuildRequires: python-rpm-macros
BuildRequires: python2-rpm-macros

# build dependencies
BuildRequires: python-setuptools

# runtime dependencies (required for %check)
BuildRequires: python2-six

# testing dependencies (required for %check)
BuildRequires: python2-pytest
BuildRequires: python2-mock

%description
The `gwosc` package provides an interface to querying the open data
releases hosted on <https://losc.ligo.org> from the LIGO and Virgo
gravitational-wave observatories.

# -- python2-gwosc

Summary:  %{summary}
Requires: python-six
%{?python_provide:%python_provide %{name}}
%description -n %{name}
The `gwosc` package provides an interface to querying the open data
releases hosted on <https://losc.ligo.org> from the LIGO and Virgo
gravitational-wave observatories.

# -- build steps

%prep
%autosetup -n %{sname}-%{version}

%build
%py2_build

%check
%{__python2} -m pytest --pyargs %{sname} -m "not remote"

%install
%py2_install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%license LICENSE
%doc README.md
%{python2_sitelib}/*

# -- changelog

%changelog
* Fri Aug 07 2020 Adam Mercer <adam.mercer@ligo.org> - 0.4.2-3
- provide python2-gwosc only

* Tue Mar 12 2019 Duncan Macleod <duncan.macleod@ligo.org> - 0.4.3-1
- bug fix release, see github releases for details

* Mon Mar 11 2019 Duncan Macleod <duncan.macleod@ligo.org> - 0.4.2-1
- bug fix release, see github releases for details

* Thu Feb 28 2019 Duncan Macleod <duncan.macleod@ligo.org> - 0.4.1-1
- development release to include catalogue parsing

* Mon Oct 1 2018 Duncan Macleod <duncan.macleod@ligo.org>
- 0.3.4 testing bug-fix release

* Mon Jul 9 2018 Duncan Macleod <duncan.macleod@ligo.org>
- 0.3.3 packaging bug-fix release
